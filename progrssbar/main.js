
    let toolsElem = document.querySelector('.tools');
    let skilssElem = document.querySelector('.skilss');
    
    
    // функция для основного прогрессбара
    function backgroundProgresBar () {
    
        toolsElem.classList.add('bar_100');
        skilssElem.classList.add('bar_100');
        
    }
    setTimeout(backgroundProgresBar, 9000);
    
    
    
    // функция для прогрессбара  веток

    let branchElem = document.querySelectorAll('.branch');

    function visibleBranchBox () {
        branchElem.forEach((itemBranch) => {
            itemBranch.style.height = 100 + 'px';
        });
    }

    setTimeout(visibleBranchBox, 9000);
    
    // функция для отображения скилов и тулзов
    
    let itemSkilElem = document.querySelectorAll('.itemSkil');
    let itemToolElem = document.querySelectorAll('.itemTool');
    
    function visibleBloks () {

        itemSkilElem.forEach((itemSkil) => {
            itemSkil.style.height = 100 + '%';
            itemSkil.style.paddingTop = 10 + 'px';
            itemSkil.style.paddingBottom = 10 + 'px';
        });

        itemToolElem.forEach((itemTool) => {
            itemTool.style.height = 100 + '%';
            itemTool.style.paddingTop = 10 + 'px';
            itemTool.style.paddingBottom = 10 + 'px';
        });

    }
    
    setTimeout(visibleBloks, 12000);

    //  функция показа заголовков

    let h6Elem = document.querySelectorAll('h6');
    console.log(h6Elem);

    function h6Visible () {
        h6Elem.forEach(function(childh6){
            childh6.style.display = 'block';
        });
    }

    setTimeout(h6Visible, 15500);