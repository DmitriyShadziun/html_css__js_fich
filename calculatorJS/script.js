
(function () {

  const DAY_STRING = ['день', 'дня', 'дней'];

  const DATA = {
    whichSite: ['landing', 'multiPage', 'onlineStore'],
    price: [4000, 8000, 26000],
    desctopeTemplates: [50, 40, 30],
    adapt: 20,
    mobileTemplates: 15,
    editable: 10,
    metrikaYandex: [500, 1000, 2000],
    analiticsGoogle: [850, 1350, 3000],
    sendOrder: 500,
    deadlineDay: [[2,7], [3,10], [7,14]],
    deadlinePercent: [20, 17, 15]
  };

 const startButton = document.querySelector('.start-button');
 const firstScreen = document.querySelector('.first-screen');
 const mainForm = document.querySelector('.main-form');
 const formCalculate = document.querySelector('.form-calculate');
 const endButton = document.querySelector('.end-button');
 const total = document.querySelector('.total');
 const fastRange = document.querySelector('.fast-range');
 const totalPriceSum = document.querySelector('.total_price__sum');
 const adapt = document.getElementById('adapt');
 const mobileTemplates = document.getElementById('mobileTemplates');
 const typeSite = document.querySelector('.type-site');
 const maxDeadline = document.querySelector('.max-deadline');
 const rangeDeadline = document.querySelector('.range-deadline');
 const deadLineValue = document.querySelector('.deadline-value');


//  функция склонения слов - возвращает чисо и правильно склоненное слово
  function declOfNum(n, titles) {
    return n + ' ' + titles[n % 10 === 1 && n % 100 !== 11 ?
      0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2];
  }


  function showElement (element) {
    element.style.display = 'block';
  }


  function hideElement (element) {
    element.style.display = 'none';
  }

  function renderTextContent (result, site, maxDeadLineDay, minDeadLineDay) {

    totalPriceSum.textContent = result;

    typeSite.textContent = site;

    maxDeadline.textContent = declOfNum(maxDeadLineDay, DAY_STRING);

    rangeDeadline.min = minDeadLineDay;
    rangeDeadline.max = maxDeadLineDay;

    deadLineValue.textContent = declOfNum(rangeDeadline.value, DAY_STRING);

  }


  function priceCalculation (target) {

    let result = 0;
    let index = 0;
    let options = [];
    let site = '';
    let maxDeadLineDay = DATA.deadlineDay[index][1];
    let minDeadLineDay = DATA.deadlineDay[index][0];

    if(target.name === 'whichSite') {
      for (const item of formCalculate.elements) {
        if(item.type === 'checkbox') {
           item.checked =  false;
        }
      }
      hideElement(fastRange);
    }

    for (const item of formCalculate.elements) {
      if(item.name === 'whichSite' && item.checked) {

        index = DATA.whichSite.indexOf(item.value);

        // typeSite.textContent = item.dataset.site; - динамическая запись в контент
        site = item.dataset.site;
        maxDeadLineDay = DATA.deadlineDay[index][1];
        minDeadLineDay = DATA.deadlineDay[index][0];
      } 
      else if (item.classList.contains('calc-handler') && item.checked) {
        options.push(item.value);
      }
    }

    options.forEach(function (key) {

      if(typeof(DATA[key]) === 'number') {

        if(key === 'sendOrder') {
          result += DATA[key];
        } else {
          result += DATA.price[index] * DATA[key] / 100;
        }
      }
       else {
        if (key === 'desctopeTemplates') {
          result += DATA.price[index] * DATA[key][index] / 100;
        } 
        else {
          result +=DATA[key][index];
        }
      }
    });

    result += DATA.price[index];

    renderTextContent(result, site, maxDeadLineDay, minDeadLineDay);

  }



  function handlerCallBackForm (event) {
    const target = event.target;

    if (adapt.checked) {
      mobileTemplates.disabled = false;
    } else {
      mobileTemplates.disabled = true;
      mobileTemplates.checked = false;
    }

    if(target.classList.contains('want-faster')) {

      target.checked ? showElement(fastRange) : hideElement(fastRange);
      
    }

    if(target.classList.contains('calc-handler')) {
      priceCalculation(target);
    }

  }


  startButton.addEventListener('click', ()=> {
    showElement(mainForm);
    hideElement(firstScreen);
  });



  endButton.addEventListener('click', ()=> {

    for (const elem of formCalculate.elements) {
      if (elem.tagName === 'FIELDSET') {
        hideElement(elem);
      }
    }

    showElement(total);

  });

  formCalculate.addEventListener('change', handlerCallBackForm);

  
}) ();