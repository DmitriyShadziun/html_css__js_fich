//Базовые селекторы

//Обращение по идентефикатору
//$('#id')

//обращение по классу
//$('.class')

//обращение по тегу
//$('div')

//обращение ко всем элементам
//$('*') - образение ко всем элементам

//можно совмещеть
// $('a#id') - обращение к ссылке а с соответствующим идентифекатором



//Взаимодействие с элементами

//Первый, второй, третий и т.д. Перечисление через запятую к которым обращаемся
//$('h2, p').css('border', '1px solid #ddd');

//outer и inner - поиск по всем элементам внутри h2. Выделит все спан внутри h2. Не зависит от таго на сколько глубоко они вложены - вернет и обратиться ко всем спан
//$('h2  span')

//parent > child - вернет значение спан, которое лежит непосредственно в h2. если есть промежуточные блоки в которых так же есть спан - он их не увидит
//$('h2 > span')

//prev + next - отмечает следующий в списке элемент, только один(даже если их там несколько). Выделит первый p cледующий за h2 - только один элемент будет выделен.
//$('h2 + p').css('свойство', 'значение');

//prev ~ next - обратиться ко всем эелеменьам p следующими за h2.
//$('h2 ~ p').css('свойство', 'значение');



// Взаимодействие по атрибутам

// Простой атрибут - выделить все элементы а с атрибутом Href
// $('a[href]').css('значение', 'value')

// Полное соответствие - выделит элемент а с атрибутом href и значением #
// $('a[href='#']').css('значение', 'value')

// Полное не соответствие - выделит все элементы которые не совпадают с указанными в атрибуте
// $('a[href!='#']').css('значение', 'value')

// Начинается с ... - выделит все ссылки у которых значение отрибута будет начинаться на http
// $('a[href ^='http://']').css('значение', 'value')

// Заканчивается на ... - выделит все ссылки у  которы\х значение атрибута заканчиваются на .ru
// $('a[href $='.ru']').css('значение', 'value')

// Содержит ... - выделит все ссылки в значении атрибута которых содержится - ya
// $('a[href *='ya']').css('значение', 'value')

// Имеет префикс - ...префикс data-target содержит атрибут main Используется для атрибутов содержащих префикс (тире)
// $('a[data-target |='main']').css('значение', 'value')

// Имеет одновременно несколько атрибутов
// $('a[href], [alt]').css('значение', 'value')



// Простые фильтры в JQuery

// Первый элемент - применится к первому параграфу
// $('p:first').css('свойство', 'значение');

// Последний элемент - применится к последнему параграфу
// $('p:last').css('свойство', 'значение');

// Элемент под номером ... (нумерация идет с 0) - применится к ссылке под номером два (фактически третьей)
// $('a:eq(2)').css('свойство', 'значение');

// Исключает из найденых элементов следующий селектор - выделятся все элементы кроме -р (элементы которые не нужно выделять перебираются через запятую)
// $('*:not(p)').css('свойство', 'значение');

// Четные найденные элементы - найдет и выделит все четные лишки
// $('li:odd').css('свойство', 'значение');

// Нечетные найденные элементы - найдет и выделит все не четные лишки
// $('li:even').css('свойство', 'значение');

// Элементы чей порядковый номер больше n - найдет и выделит все лишки которые идут после 3-ей лишки
// $('li:gt(2)').css('свойство', 'значение');

// Элементы чей порядковый номер меньше n - найдет и выделит все лишки что идут до 4-ой лишки
// $('li:де(3)').css('свойство', 'значение');

// Только те элементы, которые являются заголовками - найдет на странице ивыделит все элементы которые являются заголовками (h1-h6)
// $('*:header').css('свойство', 'значение');

// Только те элементы, что задействованны в анимации - найдет все лишки которые задействованны в анимации
// $('li:animated').css('свойство', 'значение');

// Скрытые элементы - найдет все скрытые (которые не видны пользователю на странице) ссылки -а- и обратится к ним
// $('a:hiden').css('свойство', 'значение');

// Видимые элементы - найдет и обратится ко всем видимым ссылкам -а-
// $('a:visible').css('свойство', 'значение');

// Фильтр по языку - найдет на странице все параграфы -р- в которых используется кирилица (русский язык)
// $('p:lang(ru)').css('свойство', 'значение');



// Фильтры по содержимому

// содержит текст - найдет все параграфы -р- и выделит те, в которых будет встречаться слово -lorem- - чувствительно к регистру написания
// $('p:contains(lorem)').css('свойство','значение');

// Пустой - найдет и обратится ко всем пустым дивам, и запишет в них текст (чисто для примера)
// $('div:empty').text('сам текст');

// Имеет внутри селектор - выделит весь элемент h2 внутри которого есть span
// $('h2:has(span)').css('fontSize', '30px');

// Обращение к родителям - обратится к спану с сответствующим класом имеющего родителя.
// $('span.span_parent:parent').css('fontSize', '30px');


// рабочий код
jQuery(document).ready(function($) {
    //     Взаимодействие с элементами
    //   $('h1').css('color', 'blue');
    //   $('p span').css('color', 'red');
    //   $('p.second_p > span').css('color', 'blue');
    //   $('ul > li').css('color', 'green');

    // $('li:first').css('border', '1px solid red');
    // $('li:last').css('border', '1px solid red');

    // $('div.rot:empty').text('сам текст может быть любым').css('color', 'red').css('fontSize', '30px');
    // $('h2:has(span)').css('fontSize', '30px');
    // $('span.span_parent:parent').css('fontSize', '30px');

    // Функции



    });