
let login = document.querySelector('.login');
let email = document.querySelector('.email');
let password = document.querySelector('.password');
let password2 = document.querySelector('.password2');

let submit = document.querySelector('.submit');
let reset = document.querySelector('.reset');

submit.addEventListener('click', (e) => {
    e.preventDefault(e);

    checkInputs();

    clearInputs();

});


function checkInputs () {

    // получаем данные из инпутов
    const elemLogin = login.value.trim();
    const elemEmail = email.value.trim();
    const elemPass = password.value.trim();
    const elemPass2 = password2.value.trim();

    if (elemLogin === '' || elemLogin.length >= 20 || elemLogin.length <= 5) {
        // show error
        // add class error
        errorFormLogin(elemLogin, ' User login cannot be register');
    } else {
        // add class succes
        // 
        console.log(elemLogin.length);
    }
}


function errorFormLogin(login, message) {
    // добавить контент в блок ошибки
    let errorBlock = document.querySelector('.error');
    errorBlock.textContent = login +  message;
    errorBlock.style.display = 'block'; 
    
}

function clearInputs() {
    let clear = document.querySelectorAll('.inp_clear');
    clear.forEach((itemInput) => {
        itemInput.value = '';
    });
}
