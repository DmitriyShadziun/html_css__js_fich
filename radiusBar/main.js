let circle = document.querySelector('.progress-ring__circle');
let radius = circle.r.baseVal.value;
let dlinaCruga = 2 * Math.PI * radius;


circle.style.strokeDasharray = `${dlinaCruga} ${dlinaCruga}`;
circle.style.strokeDashoffset = dlinaCruga;

function progressCircle (percent) {
 
 let offsetRing = dlinaCruga - percent / 100 * dlinaCruga;
 
 circle.style.strokeDashoffset = offsetRing;
 
}

progressCircle(75);