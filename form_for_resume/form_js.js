let writeButton = document.querySelector('.write');
let contactsBlock = document.querySelector('.contacts');
let form_contactBlock = document.querySelector('.form_contact');
let btn_closeForm = document.querySelector('.btn_close');

writeButton.addEventListener('click', hideBlock);
btn_closeForm.addEventListener('click', closeForm);

function hideBlock () {
    contactsBlock.style.display = 'none';
    form_contactBlock.style.display = 'block';
}

function closeForm () {
    contactsBlock.style.display = 'block';
    form_contactBlock.style.display = 'none';
}